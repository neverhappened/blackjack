require_relative './models/game_settings'
require_relative './models/card'
require_relative './models/dealer'
require_relative './models/hand'
require_relative './models/deck'
require_relative './models/player'

require_relative './stages/bet'
require_relative './stages/deal'
require_relative './stages/resolve'
require_relative './stages/play'
require_relative './stages/settle'
require_relative './stages/cleanup'

require_relative './models/game_initializer'
require_relative './models/game'

game = Game.new(GameSettings::NUMBER_OF_PLAYERS, GameSettings::START_MONEY)
game.play
