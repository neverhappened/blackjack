module Stages
  module Bet

    def bet_round
      puts "\n---------------\nBet round"
      players.each { |player| player_bet(player) }
    end

    private

    def player_bet(player)
      amount = player.get_input('Place your bet:').to_i
      correct_bet, error_message = player.bet!(amount)
  
      unless correct_bet
        puts error_message
        puts
        player_bet(player)
      end
    end
  
  end
end
