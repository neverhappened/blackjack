module Stages
  module Play

    def play_round(players_in_play)
      players_in_play.each { |player| player_round(player) }
      play_hit_or_stand_for_dealer
    end

    def open_double_down_cards(players_in_play)
      players_in_play.each(&:open_hands!)
    end

    private

    def player_round(player)
      play_additional_actions(player)
      play_hit_or_stand_for_player(player)
    end

    def play_additional_actions(player)
      decision = ask_for_additional_actions(player)
      if decision == :split
        second_hand = player.split_hands!
      elsif decision == :double_down
        player.double_down!(deck.deal_face_down)
      end
    end

    def play_hit_or_stand_for_player(player)
      player.hands.each do |hand|
        hit_or_stand_loop(player, hand, -> { ask_decision(player) })
      end
    end

    def play_hit_or_stand_for_dealer
      puts "\n\n---------------\n#{dealer.number_to_s} turn\n"
      hit_or_stand_loop(dealer, dealer.hand, -> { ask_dealer_decision })
    end

    def hit_or_stand_loop(someone, hand, decision_func)
      loop do
        decision = decision_func.call
        if decision == :stand
          puts "#{someone.number_to_s} #{hand.number_to_s} decided to stand with #{hand}\n"
          return
        elsif decision == :hit
          puts "#{someone.number_to_s} #{hand.number_to_s} decided to hit with #{hand}\n"
          card = deck.deal
          hand.add(card)
          if hand.bust?
            puts "#{someone.number_to_s} #{hand.number_to_s} got #{card}. Busted!\n"
            return
          else
            puts "#{someone.number_to_s} #{hand.number_to_s} got #{card}. Hand: #{hand}"
          end
        else
          raise "Unsupported decision: #{decision}"
        end
      end
    end

    def ask_decision(player)
      puts
      decision = player
        .get_input("Hit or stand? (write 'hit' or 'stand')?")
        .downcase
        .strip
        .to_sym
  
      if %I(hit stand).include?(decision)
        decision
      else
        puts 'Unsupported decision'
        ask_decision(player)
      end
    end

    def ask_dealer_decision
      decision = dealer.autodecision
      # Just so that it wont be instant in CLI!
      sleep(1.0)
      decision
    end

    def ask_for_additional_actions(player)
      available_decisions = player.available_decisions
      return if available_decisions.empty?
      available_decisions.push(:no)
      actions_str = available_decisions.map { |action| "'#{action}'"  }.join(', ')

      puts
      decision = player
        .get_input("Additional actions? (pick #{actions_str})?")
        .downcase
        .strip
        .to_sym
      if available_decisions.include?(decision)
        decision
      else
        ask_for_additional_actions(player)
      end
    end

  end
end
