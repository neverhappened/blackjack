module Stages
  module Deal
    def deal_initial_cards
      2.times do
        players.each { |player| player.first_hand.add(deck.deal) }
      end
      dealer.hand.add(deck.deal)
      dealer.hand.add(deck.deal_face_down)

      puts "---------------\nCards dealed:\n#{hands_to_s}\n"
    end  

    private

    def hands_to_s
      players.map(&:hands_to_s).join("\n") + "\n" + dealer.hands_to_s
    end
  end
end
