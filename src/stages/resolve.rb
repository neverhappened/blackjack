module Stages
  module Resolve

    # NOTE: Returns [need_to_continue_round, players_that_continue_round]
    def resolve_naturals
      puts "\n\n---------------\nResolving naturals:\n"
      natural_players, other_players = players.partition { |player| player.first_hand.natural? }

      if dealer.hand.maybe_natural?
        dealer.hand.lookup_face_down_card!
        puts 'Dealer looks up face down card'
        puts "His hand now #{dealer.hand}"

        if dealer.hand.natural?
          resolve_dealer_natural(natural_players)
        else
          resolve_simple(natural_players, other_players)
        end
      else
        resolve_simple(natural_players, other_players)
      end
    end

    def resolve_dealer_natural(natural_players)
      puts 'Dealer got natural'
      natural_players.each do |player|
        puts "#{player.number_to_s} also has natural. He gets hit bet back"
        player.return_bets!(1.0)
      end
      puts 'Other players lose their bet'
      [false, []]
    end

    def resolve_simple(natural_players, other_players)
      natural_players.each do |player|
        puts "#{player.number_to_s} got natural. He gets hit bet * 1.5"
        player.return_bets!(1.5)
      end
      [other_players.length > 0, other_players]
    end

  end
end
