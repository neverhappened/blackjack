module Stages
  module Cleanup

    def with_round_cleanup
      yield.tap do
        clean_hands
        refill_deck
        remove_losers
      end
    end

    def clean_hands
      players.each(&:clean_hands!)
      dealer.hand.hand_over!
    end

    def refill_deck
      deck.refill!
    end

    def remove_losers
      self.players, losers = players.partition(&:afford_to_play?)

      losers.each do |player|
        puts "#{player.number_to_s} is out of the game"
      end
    end
  end
end
