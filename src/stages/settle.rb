module Stages
  module Settle

    def settlement(players)
      puts "\n\n---------------\nBefore settlement: \n#{self}\n"

      dealer_busted = dealer.hand.bust?
      players.each { |player| settle_player(player, dealer_busted) }
    end

    def settle_player(player, dealer_busted)
      returns = if dealer_busted
        player.active_hands.map { |hand| [hand, 1.5] }
      else
        winners, others = player.active_hands.partition { |hand| hand.value > dealer.hand.value }
        standoffs = others.select { |hand| hand.value == dealer.hand.value }
        
        winners.map { |hand| [hand, 1.5] } + standoffs.map { |hand| [hand, 1.0] }
      end

      player.hands.select(&:bust?).each do |hand|
        puts "#{player.number_to_s} #{hand.number_to_s} got busted. Bet was $#{player.bets[hand.id]}"
      end

      returns.each do |hand, return_coeff|
        return_amount = player.bets[hand.id] * return_coeff
        puts "#{player.number_to_s} gets $#{return_amount} (#{return_coeff} coefficient)"
        player.money += return_amount
      end
    end

  end
end
