class Hand
  attr_accessor :cards, :number

  MAX_VALUE = 21

  def initialize(start_cards = [], number = 0)
    self.number = number
    self.cards = start_cards
  end

  def id
    object_id
  end

  def add(card)
    cards.push(card)
  end

  def value
    value_for_cards(cards)
  end

  def value_without_facedown
    face_up = cards.reject(&:face_down)
    value_for_cards(face_up)
  end

  def natural?
    value == MAX_VALUE
  end

  def maybe_natural?
    face_up_card = cards.reject(&:face_down)[0]
    cards.length == 2 && (face_up_card.ace? || face_up_card.ten?)
  end

  def bust?
    value > MAX_VALUE
  end

  def lookup_face_down_card!
    face_down_card = cards.select(&:face_down)[0]
    face_down_card.face_up! unless face_down_card == nil
  end

  def hand_over!
    self.cards = []
  end

  def split!
    second_card = cards.pop
    Hand.new([second_card], number + 1)
  end

  def to_s
    if cards.empty?
      "(empty hand)"
    else
      val = "%-2s" % value_without_facedown
      "(#{val}) [#{cards.join(', ')}]"
    end
  end

  def number_to_s
    "#{self.class.to_s} #{number}"
  end

  private

  def value_for_cards(cards)
    aces, simple_cards = cards.partition(&:ace?)
    base_value = simple_cards.inject(0) { |sum, card| sum + card.value }

    aces_counted = aces.count
    max_ace_value_not_busted = aces.map { 11 }.inject(0) { |sum, value| sum + value }
    while aces_counted > 0
      current_value = base_value + max_ace_value_not_busted
      if current_value > MAX_VALUE
        max_ace_value_not_busted -= 10
      end

      aces_counted -= 1
    end

    base_value + max_ace_value_not_busted
  end
end
