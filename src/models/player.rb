class Player
  attr_accessor :number, :hands, :money, :bets

  def initialize(number, hand, money)
    self.number = number
    self.hands = [hand]
    self.money = money
    self.bets = {}
  end

  # returns [correct_bet?, error_message]
  def bet!(amount)
    return [false, "Cannot bet negative amounts"] if amount <= 0
    return [false, "You dont have enough money for this. You only have $#{money}"] if money < amount
    return [false, "Cannot bet less then $#{GameSettings::MIN_BET}"] if amount < GameSettings::MIN_BET

    self.money -= amount
    self.bets[first_hand.id] = amount
    [true, nil]
  end

  def get_input(message)
    puts "[#{number_to_s}]\n#{message}"
    input = gets
    puts
    input
  end

  def afford_to_play?
    money >= GameSettings::MIN_BET
  end

  def available_decisions
    [].tap do |result|
      result.push(:split) if can_split?
      result.push(:double_down) if can_double_down?
    end
  end

  def double_down!(card)
    bets[first_hand.id] += bets[first_hand.id]
    first_hand.add(card)
  end

  def split_hands!
    first_hand.split!.tap do |second_hand|
      hands << second_hand
      bet_amount = bets[first_hand.id]
      self.money -= bet_amount
      bets[second_hand.id] = bet_amount
    end
  end

  def first_hand
    hands[0]
  end

  def return_bets!(coefficient)
    hands.each do |hand|
      amount = bets[hand.id] * coefficient
      self.money += amount
    end

    self.bets = {}
  end

  def open_hands!
    hands.each(&:lookup_face_down_card!)
  end

  def clean_hands!
    hands.each(&:hand_over!)
    self.hands = [first_hand]
    self.bets = {}
  end

  def active_hands
    hands.reject(&:bust?)
  end

  def to_s
    formatted_start = "%-10s" % "#{self.class.to_s} ##{number}"
    formatted_money = "%-7s" % "$#{money}"
    "#{formatted_start} #{formatted_money} #{hands.join(', ')}"
  end

  def money_to_s
    formatted_start = "%-10s" % "#{self.class.to_s} ##{number}"
    "#{formatted_start} $#{money}"
  end

  def number_to_s
    "#{self.class.to_s} #{self.number}"
  end

  def hands_to_s
    formatted_start = "%-10s" % "#{self.class.to_s} ##{number}"
    "#{formatted_start} #{hands.join(' | ')}"
  end

  private

  def can_split?
    raise 'Can split only on two cards' unless first_hand.cards.length == 2

    cards_are_equal_value = first_hand.cards[0].safe_value == first_hand.cards[1].safe_value
    enough_money = bets[first_hand.id] <= money
    enough_money && cards_are_equal_value
  end

  def can_double_down?
    raise 'Can double only on two cards' unless first_hand.cards.length == 2

    hand_value = first_hand.value
    enough_money = bets[first_hand.id] <= money
    correct_value = hand_value == 9 || hand_value == 10 || hand_value == 11
    enough_money && correct_value
  end
end
