class Card
  attr_accessor :suit, :rank, :face_down

  def initialize(suit, rank)
    self.suit = suit
    self.rank = rank
    self.face_down = false
  end

  def value
    raise 'Cannot access value on ace card' if ace?

    if number_rank?
      rank.to_i
    else
      10
    end
  end

  def safe_value
    if ace?
      11
    else
      value
    end
  end

  def face_down!
    self.face_down = true
  end

  def face_up!
    self.face_down = false
  end

  def number_rank?
    match = /[1-9]+/ =~ rank
    match != nil
  end

  def ace?
    rank == 'ace'
  end

  def ten?
    value == 10
  end

  def to_s
    if face_down
      "**"
    else
      "#{rank_to_s}#{suit_to_s}"
    end
  end

  private

  def rank_to_s
    number_rank? ? rank : rank.capitalize[0]
  end

  # TODO: rewrite with 'case when else'
  def suit_to_s
    if suit == 'spades'
      '♠'
    elsif suit == 'hearts'
      '♥'
    elsif suit == 'diamonds'
      '♦'
    elsif suit == 'clubs'
      '♣'
    else
      raise "Not supported card suit: #{suit}"
    end
  end
end
