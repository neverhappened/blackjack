class Dealer
  attr_accessor :hand

  MIN_STAND_VALUE = 17

  def initialize(hand)
    self.hand = hand
  end

  def autodecision
    hand.lookup_face_down_card!
    hand.value >= MIN_STAND_VALUE ? :stand : :hit
  end

  def to_s
    formatted_start = "%-18s" % self.class.to_s
    "#{formatted_start} #{hand}"
  end

  def number_to_s
    self.class.to_s
  end

  def hands_to_s
    formatted_start = "%-10s" % self.class.to_s
    "#{formatted_start} #{hand}"
  end
end
