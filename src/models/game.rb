class Game
  include Stages::Bet
  include Stages::Deal
  include Stages::Resolve
  include Stages::Play
  include Stages::Settle
  include Stages::Cleanup

  attr_accessor :deck, :players, :dealer

  def initialize(number_of_players, start_money)
    self.deck, self.players, self.dealer =
      GameInitializer.new.start_state(number_of_players, start_money)
  end

  def play
    clear_screen
    round_loop { round }
  end

  private

  def round
    with_round_cleanup do
      bet_round
      deal_initial_cards
      needs_play, players_in_play = resolve_naturals

      if needs_play
        play_round(players_in_play)
        open_double_down_cards(players_in_play)
        settlement(players_in_play)
      end

      game_end?
    end
  end

  def round_loop
    round = 0
    loop do
      puts "\n\n================\nRound #{round} started!\n\n\n"
      puts "---------------\nMoney:\n#{money_to_s}\n\n"

      game_end = yield

      if game_end
        puts "\n\n================\nGame ends"
        break
      else
        puts "\n\n---------------\nEnd of the round #{round}"
      end
      round += 1
    end
  end

  def clear_screen
	  print %x{clear}
  end

  def game_end?
    players.length == 0
  end

  def to_s
    <<-STR
#{players.join("\n")}
#{dealer}
    STR
  end

  def money_to_s
    players.map(&:money_to_s).join("\n")
  end

end
