class GameSettings

  # Minimal bet for a player. Player is out of the game if his money is less then MIN_BET
  MIN_BET = 5

  # Cards limit when deck is refilled to its full amount (DECKS_NUMBER cards)
  CARDS_TO_REFILL = 30

  # Number of 'decks' in a deck. Total cards length is 52 * DECKS_NUMBER
  DECKS_NUMBER = 3

  # Number of players in game
  NUMBER_OF_PLAYERS = 2

  # Start amount of money each player has
  START_MONEY = 100

end
