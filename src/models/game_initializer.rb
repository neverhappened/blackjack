class GameInitializer
  def start_state(number_of_players, start_money)
    deck = Deck.new(GameSettings::DECKS_NUMBER)
    players = initialize_players(number_of_players, start_money)
    dealer = initialize_dealer

    [deck, players, dealer]
  end

  private

  def initialize_players(number_of_players, start_money)
    hands = Array.new(number_of_players).map { Hand.new }
    hands.each_with_index.map { |hand, index| Player.new(index, hand, start_money) }
  end

  def initialize_dealer
    Dealer.new(Hand.new([]))
  end

end
