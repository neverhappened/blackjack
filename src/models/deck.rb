class Deck
  attr_accessor :cards, :suits, :ranks

  def initialize(number_of_decks)
    self.suits = %W[diamonds hearts spades clubs]
    self.ranks = %w[2 3 4 5 6 7 8 9 10 jack queen king ace] * number_of_decks

    refill_cards
  end

  def deal
    cards.pop
  end

  def deal_face_down
    card = cards.pop.tap do |c|
      c.face_down!
    end
  end

  def refill!
    refill_cards if needs_refill?
  end

  def to_s
    cards.join(', ')
  end

  private

  def refill_cards
    # TODO: use amount_of_decks
    self.cards = suits
      .product(ranks)
      .map { |suit, rank| Card.new(suit, rank) }
      .shuffle
  end

  def needs_refill?
    cards_left <= GameSettings::CARDS_TO_REFILL
  end

  def cards_left
    cards.length
  end
end
