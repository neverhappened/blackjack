require 'test/unit'
require_relative '../../src/models/card.rb'
require_relative '../../src/models/dealer.rb'

class DealerTest < Test::Unit::TestCase

  DEFAULT_SUIT = 'spades'

  def test_autodecision
    card = Card.new(DEFAULT_SUIT, '5')
    card2 = Card.new(DEFAULT_SUIT, '7')
    dealer = Dealer.new(Hand.new([card, card2]))
    assert_equal(dealer.autodecision, :hit)

    dealer.hand.add(Card.new(DEFAULT_SUIT, '4'))
    assert_equal(dealer.autodecision, :hit)

    dealer.hand.add(Card.new(DEFAULT_SUIT, '2'))
    assert_equal(dealer.autodecision, :stand)
  end
end
