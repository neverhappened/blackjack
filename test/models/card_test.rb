require 'test/unit'
require_relative '../../src/models/card.rb'

class CardTest < Test::Unit::TestCase

  DEFAULT_SUIT = 'spades'

  def test_number_rank?
    card = Card.new(DEFAULT_SUIT, '7')
    assert_equal(card.number_rank?, true)

    card = Card.new(DEFAULT_SUIT, 'queen')
    assert_equal(card.number_rank?, false)
  end

  def test_value
    card = Card.new(DEFAULT_SUIT, '7')
    assert_equal(card.value, 7)

    card = Card.new(DEFAULT_SUIT, 'ace')
    assert_raise(RuntimeError) { card.value }

    card = Card.new(DEFAULT_SUIT, 'king')
    assert_equal(card.value, 10)
  end

  def test_ace?
    card = Card.new(DEFAULT_SUIT, 'ace')
    assert_equal(card.ace?, true)

    card = Card.new(DEFAULT_SUIT, 'king')
    assert_equal(card.ace?, false)
  end

  def test_ten?
    card = Card.new(DEFAULT_SUIT, '10')
    assert_equal(card.ten?, true)
    
    card = Card.new(DEFAULT_SUIT, '9')
    assert_equal(card.ten?, false)

    card = Card.new(DEFAULT_SUIT, 'jack')
    assert_equal(card.ten?, true)
  end
end
