require 'test/unit'
require_relative '../../src/models/game_settings.rb'
require_relative '../../src/models/deck.rb'

class DeckTest < Test::Unit::TestCase

  def test_suits_ranks_initialization
    deck = Deck.new(1)
    assert_equal(deck.suits.length, 4)
    assert_equal(deck.ranks.length, 13)
  end

  def test_cards_initialization
    deck = Deck.new(2)
    assert_equal(deck.cards.length, 52 * 2)
  end

  def test_deal
    deck = Deck.new(2)
    assert_equal(deck.cards.length, 52 * 2)
    card = deck.deal
    assert_equal(deck.cards.length, 52 * 2 - 1)
    assert_not_nil(card)
    assert_equal(card.face_down, false)
  end

  def test_deal_face_down
    deck = Deck.new(2)
    card = deck.deal_face_down
    assert_equal(card.face_down, true)
  end

  def test_refill
    deck = Deck.new(1)
    deck.cards = deck.cards.take(31)
    deck.refill!
    assert_equal(deck.cards.length, 31)
    deck.deal
    deck.refill!
    assert_equal(deck.cards.length, 52)
  end
end
