require 'test/unit'
require_relative '../../src/models/card.rb'
require_relative '../../src/models/hand.rb'

class HandTest < Test::Unit::TestCase

  DEFAULT_SUIT = 'spades'

  def test_empty_hand
    hand = Hand.new([])
    assert_equal(hand.value, 0)
  end

  def test_facedown_hand
    card = Card.new(DEFAULT_SUIT, 'ace')
    face_down_card = Card.new(DEFAULT_SUIT, '5')
    face_down_card.face_down!
    hand = Hand.new([card, face_down_card])

    assert_equal(hand.value, 16)
    assert_equal(hand.value_without_facedown, 11)
  end

  def test_ace
    hand = Hand.new([
      Card.new(DEFAULT_SUIT, 'ace'),
      Card.new(DEFAULT_SUIT, 'king'),
    ])
    assert_equal(hand.value, 21)

    hand.add(Card.new(DEFAULT_SUIT, '6'))
    assert_equal(hand.value, 17)

    hand.add(Card.new(DEFAULT_SUIT, 'ace'))
    assert_equal(hand.value, 18)
  end

  def test_bust
    hand = Hand.new([
      Card.new(DEFAULT_SUIT, 'king'),
      Card.new(DEFAULT_SUIT, 'king')
    ])

    assert_equal(hand.bust?, false)
    hand.add(Card.new(DEFAULT_SUIT, '2'))
    assert_equal(hand.bust?, true)
  end

  def test_maybe_natural
    card = Card.new(DEFAULT_SUIT, 'ace')
    face_down_card = Card.new(DEFAULT_SUIT, '5')
    face_down_card.face_down!
    hand = Hand.new([card, face_down_card])

    assert_equal(hand.maybe_natural?, true)

    card = Card.new(DEFAULT_SUIT, 'king')
    face_down_card = Card.new(DEFAULT_SUIT, '5')
    face_down_card.face_down!
    hand = Hand.new([card, face_down_card])
    assert_equal(hand.maybe_natural?, true)

    card = Card.new(DEFAULT_SUIT, '9')
    face_down_card = Card.new(DEFAULT_SUIT, '5')
    face_down_card.face_down!
    hand = Hand.new([card, face_down_card])
    assert_equal(hand.maybe_natural?, false)
  end

  def test_natural
    hand = Hand.new([
      Card.new(DEFAULT_SUIT, 'ace'),
    ])
    assert_equal(hand.natural?, false)

    hand.add(Card.new(DEFAULT_SUIT, 'king'))
    assert_equal(hand.natural?, true)
  end
end
