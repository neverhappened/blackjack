require 'test/unit'
require_relative '../../src/models/hand.rb'
require_relative '../../src/models/player.rb'

class PlayerTest < Test::Unit::TestCase

  DEFAULT_SUIT = 'spades'

  def test_bet
    player = Player.new(1, Hand.new([], 1), 100)
    is_correct, error_message = player.bet!(10)
    assert_equal(is_correct, true)
    assert_nil(error_message)

    is_correct, _error_message = player.bet!(110)
    assert_equal(is_correct, false)

    is_correct, _error_message = player.bet!(-10)
    assert_equal(is_correct, false)    

    is_correct, _error_message = player.bet!(2)
    assert_equal(is_correct, false)
  end

  def test_afford_to_play
    player = Player.new(1, Hand.new([], 1), 100)
    assert_equal(player.afford_to_play?, true)

    player = Player.new(1, Hand.new([], 1), 2)
    assert_equal(player.afford_to_play?, false)
  end

  def available_decisions
    splittable_hand = Hand.new(
      [Card.new(DEFAULT_SUIT, '8'), Card.new(DEFAULT_SUIT, '8')],
      1,
    )
    player = Player.new(1, splittable_hand, 100)
    player.bet!(50)
    assert_equal(player.available_decisions, [:split])

    double_downable_hand = Hand.new(
      [Card.new(DEFAULT_SUIT, '8'), Card.new(DEFAULT_SUIT, '2')],
      1,
    )
    player = Player.new(1, splittable_hand, 100)
    player.bet!(20)
    assert_equal(player.available_decisions, [:double_down])

    player = Player.new(1, splittable_hand, 10)
    player.bet!(7)
    assert_equal(player.available_decisions, [])
  end

  def test_double_down
    double_downable_hand = Hand.new(
      [Card.new(DEFAULT_SUIT, '8'), Card.new(DEFAULT_SUIT, '2')],
      1,
    )
    new_card = Card.new(DEFAULT_SUIT, '5')
    new_card.face_down!
    player = Player.new(1, double_downable_hand, 100)
    player.bet!(20)
    assert_equal(player.first_hand.cards.length, 2)
    player.double_down!(new_card)
    assert_equal(player.first_hand.cards.length, 3)
    assert_equal(player.bets[player.first_hand.id], 40)
  end

  def test_split_hands
    splittable_hand = Hand.new(
      [Card.new(DEFAULT_SUIT, '8'), Card.new(DEFAULT_SUIT, '8')],
      1,
    )
    player = Player.new(1, splittable_hand, 100)
    player.bet!(50)

    assert_equal(player.hands.length, 1)
    assert_equal(player.money, 50)

    second_hand = player.split_hands!

    assert_equal(player.hands.length, 2)
    assert_equal(player.money, 0)
    assert_equal(player.bets[second_hand.id], 50)
  end

  def test_return_bets
    player = Player.new(1, Hand.new([], 1), 100)
    player.bet!(50)

    assert_equal(player.money, 50)
    player.return_bets!(1)
    assert_equal(player.money, 100)
    assert_equal(player.bets, {})
  end

  def test_open_hands
    card = Card.new(DEFAULT_SUIT, '5')
    card.face_down!
    player = Player.new(1, Hand.new([card], 1), 100)
    player.open_hands!
    assert_equal(card.face_down, false)
  end

  def test_clean_hands
    card = Card.new(DEFAULT_SUIT, '5')
    card2 = Card.new(DEFAULT_SUIT, '5')
    first_hand = Hand.new([card, card2], 1)
    player = Player.new(1, first_hand, 100)
    player.bet!(20)
    player.split_hands!
    player.clean_hands!
    assert_equal(player.bets, {})
    assert_equal(player.hands, [first_hand])
    assert_equal(first_hand.cards, [])
  end
end
